package de.jensd.extractordemo.cell;

import de.jensd.extractordemo.data.PersonBean;
import javafx.scene.control.ListCell;

/**
 *
 * @author Jens Deters
 */
public class PersonListCell extends ListCell<PersonBean>
{

   @Override
   public void updateItem(PersonBean person, boolean empty)
   {
      super.updateItem(person, empty);
      if (!empty && (person != null))
      {
         setText(String.format("%s %s (%d)", person.getFirstName(), person.getLastName(), person.getDateOfBirth().ageBinding().get()));
      }
      else
      {
         setText(null);
      }
   }
}