package de.jensd.extractordemo.cell;

import de.jensd.extractordemo.DateFormatter;
import java.time.LocalDate;
import javafx.scene.control.TableCell;

/**
 *
 * @author Jens Deters
 */
public class DateTableCell<S, T> extends TableCell<S, LocalDate> {

    public DateTableCell() {
        setStyle("-fx-alignment: CENTER-RIGHT;");
    }
    
    @Override
    protected void updateItem(LocalDate birthday, boolean empty) {
        super.updateItem(birthday, empty);
        if (!empty && (birthday != null)) {
            setText(birthday.format(DateFormatter.DEFAULT_DATE));
        } else {
            setText(null);
        }
    }

}
