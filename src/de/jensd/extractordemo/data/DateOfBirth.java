package de.jensd.extractordemo.data;

import de.jensd.extractordemo.YetAnotherDateUtil;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import javafx.beans.binding.ObjectBinding;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;

/**
 *
 * @author Jens Deters
 */
public class DateOfBirth {

    private ObjectProperty<LocalDate> birthday;
    private ObjectBinding<Long> age;
    private ObjectBinding<LocalDate> nextBirthday;
    private ObjectBinding<Long> daysToNextBirthday;

    public DateOfBirth(LocalDate birthday) {
        setValue(birthday);
    }

    public final ObjectProperty<LocalDate> birthdayProperty() {
        if (birthday == null) {
            birthday = new SimpleObjectProperty<>();
        }
        return birthday;
    }

    public final LocalDate getValue() {
        return birthdayProperty().get();
    }

    public final void setValue(final java.time.LocalDate dateOfBirth) {
        birthdayProperty().set(dateOfBirth);

    }

    public final ObjectBinding<Long> ageBinding() {
        if (age == null) {
            age = new ObjectBinding<Long>() {
                {
                    bind(birthdayProperty());
                }

                @Override
                protected Long computeValue() {
                    if (birthdayProperty().get()== null) {
                        return null;
                    }
                    return birthdayProperty().get().until(LocalDate.now(), ChronoUnit.YEARS);
                }
            };
        }
        return age;
    }

    public final ObjectBinding<LocalDate> nextBirthdayBinding() {
        if (nextBirthday == null) {
            nextBirthday = new ObjectBinding<LocalDate>() {
                {
                    bind(birthdayProperty());
                }

                @Override
                protected LocalDate computeValue() {
                    if (birthdayProperty().get() == null) {
                        return null;
                    }
                    return YetAnotherDateUtil.getNextBirthday(birthdayProperty().get());
                }
            };
        }
        return nextBirthday;
    }

    public final ObjectBinding<Long> daysToNextBirthdayBinding() {
        if (daysToNextBirthday == null) {
            daysToNextBirthday = new ObjectBinding<Long>() {
                {
                    bind(birthdayProperty());
                }

                @Override
                protected Long computeValue() {
                    if (birthdayProperty().get() == null) {
                        return null;
                    }
                    return YetAnotherDateUtil.getDaysToNextBirthday(birthdayProperty().get());
                }
            };
        }
        return daysToNextBirthday;
    }
}
