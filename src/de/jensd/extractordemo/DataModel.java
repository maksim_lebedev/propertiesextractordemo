package de.jensd.extractordemo;

import de.jensd.extractordemo.data.DataSource;
import de.jensd.extractordemo.data.PersonBean;
import de.jensd.extractordemo.data.PersonPojoBean;
import javafx.collections.ObservableList;

/**
 *
 * @author Jens Deters
 */
public class DataModel {

    private ObservableList<PersonPojoBean> personPojoBeans;
    private ObservableList<PersonBean> personFXBeans;

    public DataModel() {
        init();
    }

    private void init() {
        personPojoBeans = DataSource.getRandomPersonPojoBeansList(100);
        personFXBeans = DataSource.getRandomPersonBeansList(100);
    }

    public ObservableList<PersonPojoBean> getPersonPojoBeans() {
        return personPojoBeans;
    }

    public ObservableList<PersonBean> getPersonFXBeans() {
        return personFXBeans;
    }
}
