package de.jensd.extractordemo;

import java.time.format.DateTimeFormatter;

/**
 *
 * @author Jens Deters
 */
public class DateFormatter {
    
    public final static DateTimeFormatter DEFAULT_DATE = DateTimeFormatter.ofPattern("dd.MM.yyyy");
}
