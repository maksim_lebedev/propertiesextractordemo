package de.jensd.extractordemo.comparator;

import de.jensd.extractordemo.data.PersonBean;
import java.util.Comparator;

/**
 *
 * @author Jens Deters
 */
public class DaysToNextBirthdayComparator implements Comparator<PersonBean> {

    @Override
    public int compare(PersonBean o1, PersonBean o2) {
        if (null != o1 && null != o2) {
            if (null != o1.getDateOfBirth() && null != o2.getDateOfBirth()) {
                if (null != o1.getDateOfBirth().daysToNextBirthdayBinding().get() && null != o2.getDateOfBirth().daysToNextBirthdayBinding().get()) {
                    return o1.getDateOfBirth().daysToNextBirthdayBinding().get().compareTo(o2.getDateOfBirth().daysToNextBirthdayBinding().get());
                }
            }
        }
        return 0;
    }

    @Override
    public String toString() {
        return "Days to Next Birthday";
    }
}
