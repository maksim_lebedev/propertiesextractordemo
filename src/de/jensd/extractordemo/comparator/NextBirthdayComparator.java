package de.jensd.extractordemo.comparator;

import de.jensd.extractordemo.data.PersonBean;
import java.util.Comparator;

/**
 *
 * @author Jens Deters
 */
public class NextBirthdayComparator implements Comparator<PersonBean> {

    @Override
    public int compare(PersonBean o1, PersonBean o2) {
        if (o1 != null && o2 != null) {
            if (null != o1.getDateOfBirth().nextBirthdayBinding().get() && null != o2.getDateOfBirth().nextBirthdayBinding().get()) {
                return o1.getDateOfBirth().nextBirthdayBinding().get().compareTo(o2.getDateOfBirth().nextBirthdayBinding().get());
            }
        }
        return 0;
    }

    @Override
    public String toString() {
        return "Next Birthday";
    }
}
